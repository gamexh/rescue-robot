#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <string>
#include <cmath>
#include "GPSModule.h"
#include "Location.h"
#include "PathFinder.h"
#include "IMU.h"
#include "USonic.h"

#define BufSize 1000

int main (int argc, char *argv[]){

IMU rhexIMU;
GPSModule rhexGPS;
Location loc;
PathFinder path;
USonic sensorLeft(4, 5);
USonic sensorRight(6, 10);

Location destLoc;
//58.366284, 26.694357 - Physicum front door
//58.366327, 26.694342 - Nooruse 1 Parking space near the center house
//58.366406, 26.692754 - half way between Physicum and Nooruse-1 near Chemicum
destLoc.setLocation(58.366347, 26.692508);
path.setNewDestination(destLoc);

int runRobot = 1;
if (!runRobot){
    while (1){
        rhexGPS.updateGPS();
        rhexIMU.update();

        if (rhexGPS.getFix()){

            rhexGPS.getLocation(loc);
            path.setCurrentLocation(loc);

            loc.printInfo();
            printf("---------------\n");
            rhexGPS.printInfo();
            printf("---------------\n");
            path.printInfo();
            printf("---------------\n");
            rhexIMU.printInfo();
            //printf("---------------\n");
            //printf("Left distance: %d", sensorLeft.getDistance());
            //printf("Right distance: %d", sensorRight.getDistance());
            printf("------------------------------\n");
        }
        usleep(250000);
    }

    return 0;
    }

else if (runRobot){
//    double headings[BufSize];
//    int headingPointer = 0;
//    double headingAvg = 0.0;
int counter = 0;
    while (1){
        for(int i = 0; i<125; i++){
            rhexGPS.updateGPS();
        }

        rhexIMU.update();

//        headings[headingPointer++] = rhexIMU.getHeading360();
//        if (headingPointer == BufSize){
//            headingPointer = 0;
//        }
//
//        headingAvg = 0.0;
//        for (int i = 0; i < BufSize; i++){
//            headingAvg += headings[i];
//        }
//        headingAvg /= BufSize;
//        printf("average: %f\n", headingAvg);

        if (rhexGPS.getFix()){

            rhexGPS.getLocation(loc);
            path.setCurrentLocation(loc);
        counter++;
        if(counter > 90){
            if (path.areWeThereYet(0.000050)){
                std::cout << "Yes, finaly there!" << std::endl;
                std::cout << "Yes, finaly there!" << std::endl;
                std::cout << "Yes, finaly there!" << std::endl;
                return 0;
            }

            double head = rhexIMU.getHeading180();
            double angl = path.getAngle();
            int test1 = (head < 0 ) & (angl < 0) & (std::abs(head - angl) < 15);
            int test2 = (head > 0) & (angl < 0) & (std::abs(head - angl) < 15);
            int test3 = (head < 0) & (angl > 0) & (std::abs(angl - head) < 15);
            int test4 = (head > 0 ) & (angl > 0) & (std::abs(head - angl) < 15);
            if (test1 | test2 | test3 | test4){
                //drive straight
                std::cout << "--------------------------" << std::endl;
                printf("Go straight\n");
                path.printInfo();
                std::cout << "------------" << std::endl;
                rhexIMU.printInfo();
                //rhexGPS.printInfo();
                loc.printInfo();
            }
            else{
                if (path.getAngle() < rhexIMU.getHeading180()){
                    //turn left
                    std::cout << "--------------------------" << std::endl;
                    printf("Turn left\n");
                    path.printInfo();
                    std::cout << "------------" << std::endl;
                    rhexIMU.printInfo();
                    //rhexGPS.printInfo();
                    loc.printInfo();

                }
                else{
                    //turn right
                    std::cout << "--------------------------" << std::endl;
                    printf("Turn right\n");
                    path.printInfo();
                    std::cout << "---" << std::endl;
                    rhexIMU.printInfo();
                    //rhexGPS.printInfo();
                    loc.printInfo();
                }
            }
            counter = 0;
        }

        }
    }
    return 0;
}
}
