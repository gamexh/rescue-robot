#ifndef IMU_H
#define IMU_H

#include <stdint.h>
#include "LSM9DS0.h"

//! Inertia measurement unit implementation for LSM9DS0 by STMicrolectronics.
class IMU
{
    public:
        IMU();
        virtual ~IMU();
        void readMag(int *);
        void readAcc(int *);
        void readRPY(int *);
        double getHeading();
        double getHeading180();
        void updateMag();
        void updateAcc();
        void updateRPY();
        void update();
        void setDeclinationCorrection(double);
        void printInfo();
    protected:
    private:
        int I2Cfile;
        double declinationCorrection;
        void initMag();
        void initAcc();
        void initRPY();
        void openI2C();
        void writeReg(int, uint8_t, uint8_t);
        void selectDevice(int);
        void readBlock(uint8_t, uint8_t, uint8_t *);
        int magData[3];
        int accData[3];
        int gyrData[3];
};

#endif // IMU_H
