#ifndef PATHFINDER_H
#define PATHFINDER_H

#include "Location.h"

//! This class can be used to find angle and distance between destination and current location.
class PathFinder
{
    public:
        PathFinder();
        virtual ~PathFinder();
        void setNewDestination(Location);
        void setCurrentLocation(Location);
        double getAngle();
        double getAngle360();
        double getDistance();
        void printInfo();
        bool areWeThereYet(double);
        void updateAngle();
        void updateDistance();
    protected:
    private:
        double angle;
        double distance;
        Location destinationLoc;
        Location currentLoc;
};

#endif // PATHFINDER_H
