#ifndef GPSMODULE_H
#define GPSMODULE_H

#include "nmea.h"
#include "Location.h"

//! GPSModule is used to get and parsed data received from Venus GPS module.
class GPSModule
{
    public:
        GPSModule();
        virtual ~GPSModule();
        void updateGPS();
        void tx_sentence(const char *);
        bool getLocation(Location&);
        double getLongitude();
        double getLatitude();
        double getDilution();
        double getDirection();
        double getDeclination();
        double getSpeed();
        bool getFix();
        void printInfo();
    protected:
    private:
        nmeaINFO info;
        nmeaPARSER parser;

        //Variales for UART
        int uart0_filestream;
        unsigned char rx_buffer[256];
        char buff[126];
        int buffPointer;
        int rx_lenght;
};

#endif // GPSMODULE_H
