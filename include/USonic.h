#ifndef USONIC_H
#define USONIC_H

//! Ultrasonic sensor class to measure distance.
class USonic
{
    public:
        USonic(int, int);
        virtual ~USonic();
        int getDistance();
    protected:
    private:
        int echoPin, trigPin;
};

#endif // USONIC_H
