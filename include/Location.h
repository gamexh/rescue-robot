#ifndef LOCATION_H
#define LOCATION_H

//! This class holds location information in decimal degrees format.
/*!
    A class to hold and manipulate location data containing
    longitude and latitude.
*/
class Location
{
    public:
        //! A constructor
        Location();
        //! A destructor
        virtual ~Location();
        //! A longitude getter
        /*!
            \return longitude in decimal degrees
        */
        double getLongitude();
        //! A latitude getter
        /*!
            \return latitude in decimal degrees
        */
        double getLatitude();
        void setLocation(double, double);
        void setNMEALocation(double, double);
        void printInfo();
    protected:
        bool convertToDecimal(double);
        double NMEAConvertToDecimal(double);
        bool decimaToDegrees(double);
    private:
        double lat;
        double lon;
};

#endif // LOCATION_H
