#include "USonic.h"
#include <wiringPi.h>

//! Ultrasonic class constructor.
/*!
    In this constructor Ultrasonic sensor pins are assigned and Raspberry Pi pins are initialized.
    \param EchoPin wiringPi library echo pin number.
    \param TrigPin wiringPi library trigger pin number.
*/
USonic::USonic(int EchoPin, int TrigPin)
{
    echoPin = EchoPin;
    trigPin = TrigPin;

    wiringPiSetup();
    pinMode(trigPin, OUTPUT);
    pinMode(echoPin, INPUT);

    digitalWrite(trigPin, LOW);
}

//! A destructor.
USonic::~USonic()
{
    //dtor
}

//! Get distance.
/*!
    Performes measurement with ultrasonic sensor and returns distance in cm.
    Implementation puts it's thread to sleep!
    \return Distance in cm.
*/
int USonic::getDistance(){
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(20);
    digitalWrite(trigPin, LOW);

    while(digitalRead(echoPin) == LOW);

    long startTime = micros();
    while(digitalRead(echoPin) == HIGH);
    long travelTime = micros() - startTime;

    int distance = travelTime / 58;

    return distance;
}
