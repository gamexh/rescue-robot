#include "IMU.h"
#include <stdio.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <stdint.h>
#include <math.h>

//! A constructor.
IMU::IMU()
{
    declinationCorrection = 8.6;    //declination in Tartu
    openI2C();
    initMag();
    initAcc();
    initRPY();
}

//! A destructor.
IMU::~IMU()
{

}

//! Read magnetometer values.
/*!
    Using I2C read magnetometer data from LSM9DS0 registers for all three axes.
    \param *magDataSaveVar Array for three magnetic values. X, Y and Z axis.
    \sa readBlock(), selectDevice()
*/
void IMU::readMag(int *magDataSaveVar){
    selectDevice(MAG_ADDRESS);

    uint8_t block[6];
    readBlock(0x80 | OUT_X_L_M, sizeof(block), block);

    *magDataSaveVar = (int16_t)(block[0] | block[1] << 8);
    *(magDataSaveVar+1) = (int16_t)(block[2] | block[3] << 8);
    *(magDataSaveVar+2) = (int16_t)(block[4] | block[5] << 8);
}

//! Read accelerometer values.
/*!
    Using I2C read accelerometer data from LSM9DS0 registers for all three axes.
    \param *accDataSaveVar Array for three acceleration values. X, Y and Z axis.
    \sa readBlock(), selectDevice()
*/
void IMU::readAcc(int *accDataSaveVar){
    selectDevice(ACC_ADDRESS);

    uint8_t block[6];
    readBlock(0x80 | OUT_X_L_A, sizeof(block), block);

    *accDataSaveVar = (int16_t)(block[0] | block[1] << 8);
    *(accDataSaveVar+1) = (int16_t)(block[2] | block[3] << 8);
    *(accDataSaveVar+2) = (int16_t)(block[4] | block[5] << 8);
}

//! Read gyro rotation values.
/*!
    Using I2C read gyro data from LSM9DS0 registers for all three rotations.
    \param *magDataSaveVar Array for row, pitch and yaw values.
    \sa readBlock(), selectDevice()
*/
void IMU::readRPY(int *gyrDataSaveVar){
    selectDevice(GYR_ADDRESS);

    uint8_t block[6];
	readBlock(0x80 | OUT_X_L_G, sizeof(block), block);

    *gyrDataSaveVar = (int16_t)(block[0] | block[1] << 8);
    *(gyrDataSaveVar+1) = (int16_t)(block[2] | block[3] << 8);
    *(gyrDataSaveVar+2) = (int16_t)(block[4] | block[5] << 8);
}

//! Find true north.
/*!
    Function calculates magnetic north using titlted compass algorithm and compensates for declination.
    \return True north in degrees from 0 - 360.
    \sa update(), updateMag(), updateAcc(), setDeclinationCorrection()
*/
double IMU::getHeading(){
    double accXnorm,accYnorm,pitch,roll,magXcomp,magYcomp;

    accXnorm = accData[0]/sqrt(accData[0] * accData[0] + accData[1] * accData[1] + accData[2] * accData[2]);
    accYnorm = accData[1]/sqrt(accData[0] * accData[0] + accData[1] * accData[1] + accData[2] * accData[2]);
    pitch = asin(accXnorm);
    roll = -asin(accYnorm/cos(pitch));
    magXcomp = magData[0]*cos(pitch)+magData[02]*sin(pitch);
    magYcomp = magData[0]*sin(roll)*sin(pitch)+magData[1]*cos(roll)-magData[2]*sin(roll)*cos(pitch);

    double heading = 180*atan2(magYcomp,magXcomp)/M_PI;
    heading -= declinationCorrection;

    if(heading < 0){
        heading += 360;
    }
    else if (heading > 360){
        heading -= 360;
    }
    return heading;
}

double IMU::getHeading180(){
    double angle360 = getHeading();
    if (angle360 > 180){
        return angle360 -360;
    }
    return angle360;
}

//! Update magnetic data from magnetometer.
/*!
    \sa readMag()
*/
void IMU::updateMag(){
    readMag(magData);
}

//! Update acceleration data from accelerometer.
/*!
    \sa readAcc()
*/
void IMU::updateAcc(){
    readAcc(accData);
}

//! Update rotation data from gyroscope.
/*!
    \sa readRPY()
*/
void IMU::updateRPY(){
    readRPY(gyrData);
}

//! Update magnetic, acceleration and rotation data
/*!
    \sa readMag(), readAcc(), readRPY(), updateMag(), updateAcc(), updateRPY()
*/
void IMU::update(){
    updateMag();
    updateAcc();
    updateRPY();
}

//! Set magnetic declination error.
/*!
    This value is later used to correct the heading.
    \param declination Magnetic declination error.
    \sa getHeading()
*/
void IMU::setDeclinationCorrection(double declination){
    declinationCorrection = declination;
}

//! Initialize LSM9DS0 magnetometer registers.
/*!
    Modify this function to get different functionality from magnetometer.
*/
void IMU::initMag(){
    writeReg(MAG_ADDRESS, CTRL_REG5_XM, 0b11110000);
    writeReg(MAG_ADDRESS, CTRL_REG6_XM, 0b01100000);
    writeReg(MAG_ADDRESS, CTRL_REG7_XM, 0b00000000);
}

//! Initialize LSM9DS0 accelerometer registers.
/*!
    Modify this function to get different functionality from accelerometer.
*/
void IMU::initAcc(){
    writeReg(ACC_ADDRESS, CTRL_REG1_XM, 0b01100111);
    writeReg(ACC_ADDRESS, CTRL_REG2_XM, 0b00100000);
}

//! Initialize LSM9DS0 gyroscope registers.
/*!
    Modify this function to get different functionality from gyroscope.
*/
void IMU::initRPY(){
    writeReg(GYR_ADDRESS, CTRL_REG1_G, 0b00001111);
    writeReg(GYR_ADDRESS, CTRL_REG4_G, 0b00110000);
}

//! Open I2C for communication.
void IMU::openI2C(){
    char filename[20];
    sprintf(filename, "/dev/i2c-%d", 1);
    I2Cfile = open(filename, O_RDWR);
    if (I2Cfile < 0){
        printf("IMU: I2C not opened.");
    }
}

//! write to I2C device register.
/*!
    This function uses i2C-dev library to write data to i2c slave registers. Printf() used to display error in case of one.
    \param device Device number to whom to send the data. (MAG_ADDRESS, ACC_ADDRESS, GYR_ADDRESS)
    \param reg Device register where to send the data.
    \param value Data to be sent.
    \sa selectDevice()
*/
void IMU::writeReg(int device, uint8_t reg, uint8_t value){
    selectDevice(device);
    int result = i2c_smbus_write_byte_data(I2Cfile, reg, value);
    if (result == -1){
        printf ("IMU: Failed to write byte to I2C.");
    }
}

//! Select i2c device
/*!
    Used to select the device to whom to start the communication.
    \param addr Device addres. (MAG_ADDRESS, ACC_ADDRESS, GYR_ADDRESS)
    \sa writeReg(), readMag(), readAcc(), readRPY()
*/
void IMU::selectDevice(int addr){
    if (ioctl(I2Cfile, I2C_SLAVE, addr) < 0) {
        printf("Failed to select I2C device.");
    }
}

//! Read data from i2c.
/*!
    Reads given amount of data from i2c device.
    \param command Start register address from where the reading starts.
    \param size Amount of data to be read from the device. (NOTE: sizeof(data))
    \param *data Pointer to the variable where the received data will be saved.
    \sa readMag(), readAcc(), readRPY()
*/
void IMU::readBlock(uint8_t command, uint8_t size, uint8_t *data){
    int result = i2c_smbus_read_i2c_block_data(I2Cfile, command, size, data);
    if (result != size)
    {
       printf("IMU: Failed to read block from I2C.");
    }
}

void IMU::printInfo(){
    printf("Heading angle: %f (360), %f (+-180) \n", getHeading(), getHeading180());
}
