#include "Location.h"
#include <cmath>
#include <stdio.h>

Location::Location()
{
    lat = 0.0;
    lon = 0.0;
}

Location::~Location()
{
    //dtor
}

double Location::getLongitude(){
    return lon;
}

double Location::getLatitude(){
    return lat;
}

//! A lattitude and longitude setter.
/*!
    \param latitude latitude in decimal degrees.
    \param longitude longitude in decimal degrees.
    \sa setNMEALocation()
*/
void Location::setLocation(double latitude, double longitude){
    lat = latitude;
    lon = longitude;
}

//! A lattitude and longitude setter from NMEA standard format.
/*!
    \param latitude latitude in decimal minutes.
    \param longitude longitude in decimal minutes.
    \sa setLocation()
*/
void Location::setNMEALocation(double latitude, double longitude){
    lat = NMEAConvertToDecimal(latitude);
    lon = NMEAConvertToDecimal(longitude);
}

//! Convert NMEA decimal minutes to decimal degrees.
/*!
    \param LatOrLon value to be converted from decimal minutes to decimal degrees.
    \return Converted value in decimal degrees.
    \sa setNMEALocation()
*/
double Location::NMEAConvertToDecimal(double LatOrLon){
    double deg = floor(LatOrLon / 100);
    double minSec = (LatOrLon - (deg * 100)) / 60;
    return (deg + minSec);
}

//! Print longitude and latitude.
/*!
    Prints out data to console with printf() function.
*/
void Location::printInfo(){
    printf("Longitude: %f\n", lon);
    printf("Latitude: %f\n", lat);
}
