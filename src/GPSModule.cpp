#include "GPSModule.h"
#include "nmea.h"
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include "Location.h"

//! GPS module constructor.
/*!
    Initialize UART or other communications port in here.
*/
GPSModule::GPSModule()
{

    nmea_zero_INFO(&info);
    nmea_parser_init(&parser);

    //Set up the UART port
    //---------------------------------------------------
    uart0_filestream = -1;
    uart0_filestream = open("/dev/ttyAMA0", O_RDONLY | O_NOCTTY | O_NDELAY);

    if (uart0_filestream == -1){
    printf("Error - Unable to open UART");
    }

    struct termios options;
    options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    tcflush(uart0_filestream, TCIFLUSH);
    tcsetattr(uart0_filestream, TCSANOW, &options);
    //---------------------------------------------------

    buffPointer = 0;
}

//! A destructor.
GPSModule::~GPSModule()
{
    nmea_parser_destroy(&parser);
}

//! Catch complete NMEA sentences and parse them.
/*!
    Use this function to send NMEA sentences to the parser.\n
    Make sure that the sentence has the following form
    ${data}\\r\\n\0
    \param *Sentence NMEA string that needs to be parsed.
*/
void GPSModule::tx_sentence(const char *Sentence){
    nmea_parse(&parser, Sentence, (int)strlen(Sentence), &info);
}

//! A Location getter
/*!
    Gets the latest locations parsed from received sentences.\n
    It doesn't return new location if GPS module does not have a GPS coordinate fix.
    \param &Loc Reference to location variable where to save new location.
    \return True if new location was saved.
            False if new location was not saved.
    \sa getFix(), getLongitude(), getLatitude()
*/
bool GPSModule::getLocation(Location &loc){
    if (info.sig != 0){
        loc.setNMEALocation(info.lat, info.lon);
//        locData[0] = info.lat;
//        locData[1] = info.lon;
        return true;
    }
    return false;
}

//! Update GPS module.
/*!
    Run this functon as frequently as possible to read data from
    communications port and parse GPS data.\n\n
    Implement UART or other communications port read in here.
*/
void GPSModule::updateGPS(){

    //Implement communications port read
    //---------------------------------------------
    if (uart0_filestream != -1){
        rx_lenght = read(uart0_filestream, (void*)rx_buffer, 255);
        if (rx_lenght < 0){

        }
        else if (rx_lenght == 0){

        }
        else{
            for (int i = 0; i < rx_lenght; i++){
                switch (rx_buffer[i]){
                    case '$':
                        buff[0] = '$';
                        buffPointer = 1;
                        break;
                    case '\n':
                        buff[buffPointer++] = '\n';
                        buff[buffPointer++] = '\0';
                        //Always send fully constructed NMEA message to the parser.
                        tx_sentence(buff);
                        break;
                    default:
                        buff[buffPointer++] = rx_buffer[i];
                }
            }
        }
    }
    else{
        printf("No GPS connection");
    }
    //---------------------------------------------
}

//! Check if there is GPS fix
/*!
    \return True if there is a GPS fix.
            False if there is no GPS fix.
    \sa _nmeaINFO::sig
*/
bool GPSModule::getFix(){
    return info.sig;
}

//! Get longitude.
/*!
    \return Current longitude in decimal minutes.
*/
double GPSModule::getLongitude(){
    return info.lon;
}

//! Get latitude.
/*!
    \return Current latitude in decimal minutes.
*/
double GPSModule::getLatitude(){
    return info.lat;
}

//! Get moving directon in degrees. (True north)
/*!
    Returns inaccurate data if not or slow moving.
    \return Current moving directon in 0-360 degrees.
*/
double GPSModule::getDirection(){
    return info.direction;
}

//! Get moving speed.
/*!
    Returns inaccurate data if not or slow moving.
    \return Current moving speed in km/h.
*/
double GPSModule::getSpeed(){
    return info.speed;
}

//! Get declination.
/*!
    Difference between true north and magnetic north.
    \return Declination in degrees.
*/
double GPSModule::getDeclination(){
    return info.declination;
}

//! Get dilution.
/*!
    Qualitative measure to indicate error.\n
    1 < Ideal\n
    1 - 2 Excellent\n
    2 - 5 Good\n
    5 - 10 Moderate\n
    10 - 20 Fail\n
     over 20 Poor
    \return Dillusion in longitude and latitude.
*/
double GPSModule::getDilution(){
    return info.PDOP;
}

//! Print info to console.
/*!
    Uses printf() function.\n\n
    Some values may indicate inaccurate data if not or slow moving.
    \sa getLocation(), getLatitude(), getLongitude(), getDilution(), getDeclination(), getDirection()
*/
void GPSModule::printInfo(){
    printf("Lat: %f, Lon: %f\n", info.lon, info.lat);
    printf("Speed: %f\n", info.speed);
    printf("Dilution: %f\n", info.PDOP);
    printf("Angle/Direction: %f\n", info.direction);
    printf("Declination: %f\n", info.declination);
}
