#include "PathFinder.h"
#include <cmath>
#include <stdio.h>

//! A constructor.
PathFinder::PathFinder()
{
    //ctor
}

//! A destructor.
PathFinder::~PathFinder()
{
    //dtor
}

//! Set a new destination.
/*!
    Function used to set a new destination where to move the robot.
    \param loc Location where to move.
    \sa Location
*/
void PathFinder::setNewDestination(Location loc){
    destinationLoc = loc;
}

//! Set current location.
/*!
    Function used to set a current location where robot is.
    \param loc Location where robot is located.
    \sa Location
*/
void PathFinder::setCurrentLocation(Location loc){
    currentLoc = loc;
}

//! Get angle in which direction robot should move.
/*!
    \return Angle in +/- 180 degrees.
    \sa updateAngle()
*/
double PathFinder::getAngle(){
    updateAngle();
    return angle;
}

//! Get angle in which direction robot should move.
/*!
    \return Angle in degrees from 0-360.
    \sa updateAngle()
*/
double PathFinder::getAngle360(){
    double angle180 = getAngle();
    if (angle180 < 0){
        return (360 - std::abs(angle180));
    }
    return angle180;
}

//! Get the distance how far robot is from it's destination.
/*!
    \return Distance from destination in meters.
    \sa updateDistance()
*/
double PathFinder::getDistance(){
    updateDistance();
    return distance;
}

//! Calaculate and update the angle information from destination and current locations.
/*!
    Angle is calculated based on moving on a sphere ie. earth's surface.
    \sa getAngle()
*/
void PathFinder::updateAngle(){
    double curLatRad = currentLoc.getLatitude()*M_PI/180;
    double curLonRad = currentLoc.getLongitude()*M_PI/180;
    double destLatRad = destinationLoc.getLatitude()*M_PI/180;
    double destLonRad = destinationLoc.getLongitude()*M_PI/180;
    double y = sin(destLonRad - curLonRad) * cos(destLatRad);
    double x = cos(curLatRad) * sin(destLatRad) -
               sin(curLatRad) * cos(destLatRad) * cos(destLonRad - curLonRad);
    angle = atan2(y, x) / M_PI*180;
}

//! Calaculate and update the distance information from destination and current locations.
/*!
    Distance is calculated based on moving on a sphere ie. earth's surface.
    \sa getDistance()
*/
void PathFinder::updateDistance(){
    double curLatRad = currentLoc.getLatitude()*M_PI/180;
    double curLonRad = currentLoc.getLongitude()*M_PI/180;
    double destLatRad = destinationLoc.getLatitude()*M_PI/180;
    double destLonRad = destinationLoc.getLongitude()*M_PI/180;
    double deltaLat = destLatRad - curLatRad;
    double deltaLon = destLonRad - curLonRad;
    double a = sin(deltaLat / 2) * sin(deltaLat / 2) +
               cos(destLatRad) * cos(curLatRad) * sin(deltaLon / 2) * sin(deltaLon / 2);
    double c = 2 * atan2(sqrt(a), sqrt(1-a));
    distance = 6371000 * c;
}

//! Print info about angle and distance to the console.
/*!
    Uses printf function.
*/
void PathFinder::printInfo(){
    printf("Distance to destination: %f\n", getDistance());
    printf("Angle to destination: %f (360), %f (180)\n", getAngle360(), getAngle());
}

//! Test if current and destinations are almost same.
/*!
    Function finds if current location ends up in a circle specified by
    destination location and radius.
    \param radius Circle radius not in meters! (GPS degree)
*/
bool PathFinder::areWeThereYet(double radius){
    return pow(currentLoc.getLatitude() - destinationLoc.getLatitude(), 2) +
    pow(currentLoc.getLongitude() - destinationLoc.getLongitude(), 2)
    < pow(radius, 2);
}
